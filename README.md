MLF2 Boot Manager
=================

The
[Boot Manager](https://bitbucket.org/uwaplmlf2/mlf2-bootmgr/downloads/bootmgr.ahx)
is a small program that runs from flash-memory and provides a menu
interface for configuring and starting missions. Use the link above to
download the hex file to your computer and install to the TT8 as
follows.

## Using Crosscut

At the PicoDOS prompt type `LO`, from the Crosscut menu bar select
*CommPort->Snd File ASCII* and download the hex file. After
downloading, PicoDOS will ask if you want to burn the firmware into
flash, answer yes.

## Using Kermit

Download and save
[this macro](https://bitbucket.org/mfkenney/mlf2-bootmgr/downloads/tt8dn.ksc)
and load it into Kermit. While connected to PicoDOS, escape back to
the Kermit prompt and type `tt8dn filename` (where filename is the
name of the hex file). After downloading, PicoDOS will ask if you want
to burn the firmware into flash, answer yes.
