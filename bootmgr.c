/*
** MLF2 Boot Manager
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include <userio.h>
#include "log.h"
#include "menu.h"
#include "nvram.h"
#include "ioports.h"
#include "timer.h"
#include "hash.h"
#include "gps.h"
#include "util.h"
#include "mctl.h"
#include "version.h"

#define LOGFILE         "bootlog.txt"

#define DIR_ENTRY       24
#define DIRBUF_SIZE     (DIR_ENTRY*1000L)

#define NV_ISSET(name, nvp)     (nv_lookup(name, (nvp)) != -1 && (nvp)->l != 0)

static void rm_files(void *call_data);
static void do_setup(void *call_data);
static void do_gps(void *call_data);
static void run(void *call_data);
static void show_datetime(void *call_data);

static char _dirbuf[DIRBUF_SIZE];

static struct group_t {
    const char *desc;
    const char *cmdfmt;
} file_groups[] = {
    {"old log files", "dir syslog*.txt /m %lx"},
    {"old ballast log files", "dir bal*.txt /m %lx"},
    {"old data files", "dir *.nc /m %lx"},
    {"old compressed data files", "dir *.ncz /m %lx"},
    {"old CSV data files", "dir *.csv /m %lx"},
    {"old S-exp data files", "dir *.sx /m %lx"},
    {"old compressed S-exp files", "dir *.sxz /m %lx"},
    {"old compressed files", "dir *.z /m %lx"},
    {"old camera images", "dir *.jpg /m %lx"},
    {"old ZIP archives", "dir *.zip /m %lx"},
    {"old TAR archives", "dir *.tgz /m %lx"},
    {"old JSON files", "dir *.jsn /m %lx"},
    {"old compressed JSON files", "dir *.jsz /m %lx"},
    {"old Nortek ADCP files", "dir *.ntk /m %lx"},
    {"old compressed Nortek ADCP files", "dir *.ntz /m %lx"},
    {NULL, NULL}};

/*
** Menu definitions
*/
static MenuEntry me_setup[] = {
    { "Remove old files",       MENTRY_FUNC,    (void*)rm_files,        &file_groups[0]},
    { "Set parameters",         MENTRY_FUNC,    (void*)do_setup,        0},
    { "Run GPS",                MENTRY_FUNC,    (void*)do_gps,          0},
};

static Menu setup_menu = { "MISSION SETUP", MENU_PERM, 0, me_setup };

static MenuEntry me_mission[] = {
    { "Simulated Mission",      MENTRY_FUNC,    (void*)run,     "sim" },
    { "Real Mission",           MENTRY_FUNC,    (void*)run,     "mission" },
};

static Menu mission_menu = { "START MISSION", MENU_POPUP, 0, me_mission };

static MenuEntry me_top[] = {
    { "Mission Setup",          MENTRY_MENU,    (void*)&setup_menu,     0},
    { "Start Mission",          MENTRY_MENU,    (void*)&mission_menu,   0},
    { "Show date/time",         MENTRY_FUNC,    (void*)show_datetime,   0},
    { "System Test",            MENTRY_FUNC,    (void*)run,     "systest"},
};

static Menu top_menu = { "MLF2 BOOT MENU", MENU_PERM, 0, me_top };


static void
show_datetime(void *call_data)
{
    time_t      now;
    struct tm   *t;

    now = RtcToCtm();
    t = localtime(&now);
    printf("%4d-%02d-%02d %02d:%02d:%02d Z\n",
           t->tm_year+1900, t->tm_mon+1, t->tm_mday,
           t->tm_hour, t->tm_min, t->tm_sec);
    printf("Clock is set by GPS when you run 'Mission Setup'->'Run GPS'\n");
}

/*
 * unlink_all - remove a series of files.
 * @dirbuf: list of filenames and sizes from DIR command
 *
 * This function parses the output of DIR /m and removes each named
 * file from the flash disk.
 */
static void
unlink_all(char *dirbuf)
{
    char        *p0, *p1;

    /*
    ** Walk though the listing and unlink each file.
    */
    p0 = dirbuf;
    while(*p0)
    {
        p1 = strchr(p0, ' ');
        if(!p1)
            break;
        *p1++ = '\0';
        log_event("Removing %s\n", p0);
        if(unlink(p0) < 0)
            log_error("unlink", "Cannot remove file %s (%d)\n", p0, errno);

        p0 += DIR_ENTRY;
    }

}

/*
 * rm_files - menu callback function to remove all data and log files.
 */
static void
rm_files(void *call_data)
{
    struct group_t      *gp = call_data;
    char                *dirbuf = &_dirbuf[0];
    char                cmd[32];

    if(!yes_or_no("Really remove all data and log files", 0))
        return;


    /*
    ** Use the /m flag with the PicoDOS DIR command to dump the directory
    ** listing into memory.  Each directory entry is a fixed size string
    ** containing the filename and filesize separated by spaces.
    */
    while(gp->desc != NULL)
    {
        printf("Listing %s ...", gp->desc);
        fflush(stdout);
        sprintf(cmd, gp->cmdfmt, (long)dirbuf);
        execstr(cmd);
        fputs("done\n", stdout);
        unlink_all(dirbuf);
        gp++;
    }

}


static void
run(void *call_data)
{
    int         error;

    /*
    ** The recovery-mode program must exist before starting a mission.
    */
    if(!strcmp((char*)call_data, "mission") && !fileexists("recover.run"))
    {
        log_error("run", "No recovery mode program found.  Please download recover.run");
        return;
    }

    log_event("Executing %s.run\n", (char*)call_data);

    /*
    ** Disable timer and close log file because if execstr() succeeds, it
    ** won't return.
    */
    shutdown_timer();
    closelog();
    error = execstr((char*)call_data);

    /*
    ** Something's wrong.  Reopen the logfile and store an error message
    ** then reset the system.
    */
    appendlog(LOGFILE);
    log_error("run", "Cannot exec %s.run (error code %d)\n", (char*)call_data,
              error);
    closelog();
    fputs("Rebooting ...\n", stdout);
    DelayMilliSecs(100L);
    Reset();
}

static void
do_gps(void *call_data)
{
    GPSdata     gps;
    long        t;

    if(!gps_init())
        return;

    t = RtcToCtm();
    gps_read_data(&gps);
    log_event("Initial fix: %c%d %2.4f / %c%d %2.4f\n",
              gps.lat.dir, gps.lat.deg,
              (double)gps.lat.min + 1e-4*gps.lat.frac,
              gps.lon.dir, gps.lon.deg,
              (double)gps.lon.min + 1e-4*gps.lon.frac);

    fputs("Waiting for updated fix (press any key to abort) ", stdout);
    fflush(stdout);
    while(!SerByteAvail() && !(gps.status == 1 || gps.status == 2))
    {
        gps_read_data(&gps);
        SerPutByte('.');
    }

    if(SerByteAvail())
    {
        (void)SerGetByte();
        fputs(" aborted\n", stdout);
    }
    else
    {
        /* One more reading to insure satellite count > 0 */
        gps_read_data(&gps);
        fputs(" done\n", stdout);
        log_event("Acquistion time: %ld seconds\n", RtcToCtm() - t);
        log_event("New fix: %c%d %2.4f / %c%d %2.4f   (%d satellites)\n",
                  gps.lat.dir, gps.lat.deg,
                  (double)gps.lat.min + 1e-4*gps.lat.frac,
                  gps.lon.dir, gps.lon.deg,
                  (double)gps.lon.min + 1e-4*gps.lon.frac,
                  gps.satellites);
        if(!gps_set_clock())
            gps_set_clock();
    }

    gps_shutdown();

}

/*
 * verify_param - verify a parameter name.
 * @name: mission parameter name.
 *
 * Verify the name of a parameter by comparing against a list of valid
 * parameters stored in the file "params.txt".  Return 1 if @name is
 * valid, otherwise 0.
 */
static char line[128];

static int
verify_param(const char *name)
{
    char                *p;
    FILE                *fp;
    static HashTable    *ptab = 0;

    /*
    ** On the first call to the function, read the parameter file
    ** from the flash disk.
    */
    if(!ptab && fileexists("params.txt"))
    {
        if((ptab = ht_create(43, HT_NOCASE)) != 0)
        {
            /*
            ** Read the parameter names and store in the hash table
            ** for later lookup.
            */
            fp = fopen("params.txt", "r");
            while(fgets(line, (int)sizeof(line), fp) != NULL)
            {
                /*
                ** The parameter name is the first string on the line, ignore
                ** any trailing characters.
                */
                if((p = strpbrk(line, " \n\t")) != NULL)
                    *p = '\0';
                ht_insert_elem(ptab, line, NULL, 0);
            }
        }
    }

    if(ptab && name && !ht_elem_exists(ptab, name))
        return 0;

    return 1;
}

/*
 * do_setup - menu callback function to set the mission parameters.
 */
static void
do_setup(void* call_data)
{
    mission_setup(verify_param);
}

/*
 * Initialize the NVRAM parameters if they do not already exist.
 * The following two parameters are used by the MLF2 programs:
 *
 *      power-cycle:  1 for normal power cycle, 0 if program
 *                    was interrupted by a watchdog reset.
 *
 *      piston:  contains the piston position at the end of the mission,
 *               the recovery-mode program can access the value and
 *               not have to re-home the piston.
 */
static void
init_nvram_params(void)
{
    int         dirty = 0;
    nv_value    nv;

    if(nv_lookup("power-cycle", &nv) == -1)
    {
        nv.l = 1;
        nv_insert("power-cycle", &nv, NV_INT, 0);
        dirty++;
    }

    if(nv_lookup("piston", &nv) == -1)
    {
        nv.l = 0;
        nv_insert("piston", &nv, NV_INT, 0);
        dirty++;
    }

    if(dirty)
    {
        printf("Please wait while NVRAM is initialized ... ");
        fflush(stdout);
        nv_write();
        printf("done\n");
    }

}

static int
stroke_watchdog(void)
{
    PET_WATCHDOG();
    return 1;
}

int
main(int ac, char *av[])
{
    int         timer_id;
    nv_value    nv;

    InitTT8(NO_WATCHDOG, TT8_TPU);
    InitCF8(CF8StdCS, CF8StdAddr);
    if(errno != 0)
        printf("\nWARNING: InitCF8 failed, error %d\n", errno);
    if(errno == -1 || errno == PiDosNoHardware)
        goto done;

    printf("\n\nMLF2 Boot Manager (%s, %s)\n", __DATE__, __TIME__);
    printf("Revision: %s\n\n", SHORT_REV);

    init_ioports();
    init_nvram();
    init_nvram_params();

    PET_WATCHDOG();

    if(fileexists(LOGFILE))
        appendlog(LOGFILE);
    else
        openlog(LOGFILE);

    log_event("Logfile open\n");

    /*
    ** Use GPS to set system clock.
    */
    if(gps_init())
    {
        DelayMilliSecs(1000L);
        if(!gps_set_clock())
            gps_set_clock();
        gps_shutdown();
    }


    PET_WATCHDOG();

    /*
    ** Check the state of the "power-cycle" NVRAM flag and act accordingly:
    **
    **  flag value                            action
    ** --------------------------------------------------------------
    **    0 (or NVRAM error)               run recovery mode program
    **   -1                                run mission program
    **    1                                show menu
    **
    */
    if(nv_lookup("power-cycle", &nv) != -1)
    {
        switch(nv.l)
        {
            case 0:
                run((void*)"recover");
                break;
            case -1:
                run((void*)"mission");
                break;
        }
    }
    else
        run((void*)"recover");


    /*
    ** Initialize the menus
    */
    top_menu.nr_entries = sizeof(me_top)/sizeof(MenuEntry);
    mission_menu.nr_entries = sizeof(me_mission)/sizeof(MenuEntry);
    setup_menu.nr_entries = sizeof(me_setup)/sizeof(MenuEntry);

    /*
    ** Initialize the timer and setup a timer handler to be called
    ** every 60 seconds to stroke the watchdog.
    */
    init_timer();
    timer_id = set_timer(60L, stroke_watchdog);

    show_menu(&top_menu, "Exit to PicoDOS");

    unset_timer(timer_id);
    shutdown_timer();

    closelog();

 done:
    ResetToPicoDOS();

    return 0;
}
